
##
# Config for app
##

# API token
token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjEyMDk2MjAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9Vc2VybmFtZUlzVkxjbyJ9.vH-G_YjX3FJk77hDLWUQwpiZMjoA6A72uYzgHXPKFgY'


# File name DB
db_name = 'test.db'


# Config SMTP
conf_smtp = {
    'smtp_user': 'SMTPUser',
    'smtp_password': 'SMTPPass',
    'encryption': 'tls',
    'smtp_host': 'SMTPHost',
    'smtp_port': 'SMTPPort'
}

email = 'example@mail.com'
