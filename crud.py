from sqlalchemy.orm import Session
from sqlalchemy import select, insert, delete, update, and_, or_, column
from models import Client, Mail, Mailing

import schemas as schemas
from utils import logger



def get_client(db: Session, client_id: int):
    try:
        return db.execute(select(Client).where(Client.id == client_id)).scalars().one()
    except Exception as e:
        logger.error('Error selecting client (client_id = {}). Description error: {}'.format(client_id, e))
        return None


def get_client_by_filter(db: Session, **kwargs):
    filters = None
    for k, v in kwargs.items():
        if k == 'phone':
            param = Client.phone
        elif k == 'phone_code':
            param = Client.phone_code
        elif k == 'tag':
            param = Client.tag
        elif k == 'timezone':
            param = Client.timezone
        elif k == 'id':
            param = Client.id
        else:
            logger.error('Error selecting client. Description error: Filter {} is not valid'.format((k, v)))
            return None

        if v[0] in ('ilike', ):
            param = column(str(param))

        if '==' == v[0]:
            f = param == v[1]
        elif '>' == v[0]:
            f = param > v[1]
        elif '<' == v[0]:
            f = param < v[1]
        elif 'ilike' == v[0]:
            f = param.ilike(v[1])
        else:
            logger.error('Error selecting client. Description error: Filter {} is not valid'.format((k, v)))
            return None

        if filters is not None:
            filters = and_(filters, f)
        else:
            filters = f

    try:
        if filters is None:
            return db.execute(select(Client)).scalars().all()
        else:
            return db.execute(select(Client).where(filters)).scalars().all()

    except Exception as e:
        logger.error('Error selecting client. Description error: {}'.format(e))
        return None


def create_client(db: Session, client: schemas.ClientCreate):
    try:
        q = insert(Client).values(**client.model_dump()).returning(Client)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error('Error creating client. Description error: {}'.format(e))
        return None


def delete_client(db: Session, client_id: int):
    try:
        q = delete(Client).where(Client.id == client_id)
        db.execute(q)
        db.commit()
        return True
    except Exception as e:
        logger.error('Error deleting client from client_id = {}. Description error: {}'.format(client_id, e))
        return False


def update_client(db: Session, update_data: schemas.Client):
    try:
        q = update(Client).where(Client.id == update_data.id).values(**update_data.model_dump()).returning(Client)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error('Error updating client from client_id = {}. Description error: {}'.format(update_data.id, e))
        return None


def get_mailing(db: Session, mailing_id: int,):
    try:
        return db.execute(select(Mailing).where(Mailing.id == mailing_id)).scalars().one()
    except Exception as e:
        logger.error('Error selecting mailing (mailing_id = ). Description error: {}'.format(mailing_id, e))
        return None


def get_mailings_by_filter(db: Session, **kwargs):
    filters = None
    for k, v in kwargs.items():
        if k == 'tag':
            param = Mailing.tag
        elif k == 'start_datetime':
            param = Mailing.start_datetime
        elif k == 'end_datetime':
            param = Mailing.end_datetime
        elif k == 'text':
            param = Mailing.text
        elif k == 'id':
            param = Mailing.id
        else:
            logger.error('Error selecting mailing. Description error: Filter {} is not valid'.format((k, v)))
            return None

        if '==' == v[0]:
            f = param == v[1]
        elif '>' == v[0]:
            f = param > v[1]
        elif '<' == v[0]:
            f = param < v[1]
        else:
            logger.error('Error selecting mailing. Description error: Filter {} is not valid'.format((k, v)))
            return None

        if filters is not None:
            filters = and_(filters, f)
        else:
            filters = f

    try:
        if filters is None:
            return db.execute(select(Mailing)).scalars().all()
        else:
            return db.execute(select(Mailing).filter(filters)).scalars().all()
    except Exception as e:
        logger.error('Error selecting mailings. Description error: {}'.format(e))
        return None


def create_mailing(db: Session, mailing: schemas.MailingCreate):
    try:
        q = insert(Mailing).values(**mailing.model_dump()).returning(Mailing)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error('Error creating mailing. Description error: {}'.format(e))
        return None


def delete_mailing(db: Session, mailing_id: int):
    try:
        q = delete(Mailing).where(Client.id == mailing_id)
        db.execute(q)
        db.commit()
        return True
    except Exception as e:
        logger.error('Error deleting mailing from client_id = {}. Description error: {}'.format(mailing_id, e))
        return False


def update_mailing(db: Session, update_data: schemas.Mailing):
    try:
        q = update(Client).where(Client.id == update_data.id).values(**update_data.model_dump()).returning(Mailing)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error('Error updating mailing from client_id = {}. Description error: {}'.format(update_data.id, e))
        return None


def get_mail(db: Session, mail_id: int,):
    return db.execute(select(Mail).where(Mail.id == mail_id))


def get_mails_by_filter(db: Session, **kwargs):
    filters = None
    for k, v in kwargs.items():
        if k == 'id_mailing':
            param = Mail.id_mailing
        elif k == 'id_client':
            param = Mail.id_client
        elif k == 'send_datetime':
            param = Mail.send_datetime
        elif k == 'status':
            param = Mail.status
        elif k == 'id':
            param = Mail.id
        else:
            logger.error('Error selecting mails. Description error: Filter {} is not valid'.format(k, v))
            return None

        if '==' == v[0]:
            f = param == v[1]
        elif '>' == v[0]:
            f = param > v[1]
        elif '<' == v[0]:
            f = param < v[1]
        else:
            logger.error('Error selecting mails. Description error: Filter {} is not valid'.format((k, v)))
            return None

        if filters is not None:
            filters = and_(filters, f)
        else:
            filters = f

    try:
        if filters is None:
            return db.execute(select(Mail)).scalars().all()
        else:
            return db.execute(select(Mail).where(filters)).scalars().all()
    except Exception as e:
        logger.error('Error selecting mails. Description error: {}'.format(e))
        return None


def create_mail(db: Session, mail: schemas.MailCreate):
    try:
        q = insert(Mail).values(**mail.model_dump()).returning(Mail)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error(
            'Error updating mailing from (client_id = {}, mailing_id = {}). Description error: {}'
            .format(mail.id_client, mail.id_mailing, e)
            )
        return None


def update_mail(db: Session, update_data: schemas.Mail):
    try:
        q = update(Mail).where(Mail.id == update_data.id).values(**update_data.model_dump()).returning(Mail)
        res = db.execute(q).one()
        db.commit()
        return res[0]
    except Exception as e:
        logger.error('Error updating mail from mail_id = {}. Description error: {}'.format(update_data.id, e))
        return None
