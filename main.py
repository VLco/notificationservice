import uvicorn

from fastapi import FastAPI

from routers import mailings, clients, events, mails


app = FastAPI(title="Notification Service", description="Small description")

app.include_router(clients.router)
app.include_router(mailings.router)
app.include_router(events.router)
app.include_router(mails.router)


if __name__ == '__main__':
    uvicorn.run("main:app", port=8008, reload=True)
