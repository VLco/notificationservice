import datetime

from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy import Integer, String, DateTime, ForeignKey, func, TIMESTAMP
from typing import Optional


class Base(DeclarativeBase):
    pass


class Client(Base):
    __tablename__ = "clients"

    id: Mapped[int] = mapped_column(Integer, index=True, primary_key=True, autoincrement=True)
    phone: Mapped[str] = mapped_column(String(11), nullable=False)
    phone_code: Mapped[str] = mapped_column(String(3), nullable=False)
    tag: Mapped[str] = mapped_column(String, nullable=False)
    timezone: Mapped[Optional[str]] = mapped_column(String)

    mails: Mapped[list["Mail"]] = relationship(back_populates="client")

    def to_dict(self):
        return {field.name: getattr(self, field.name) for field in self.__table__.c}


class Mailing(Base):
    __tablename__ = "mailings"

    id: Mapped[int] = mapped_column(Integer, index=True, primary_key=True, autoincrement=True)
    start_datetime: Mapped[int] = mapped_column(Integer, nullable=False, default=func.now())
    end_datetime: Mapped[int] = mapped_column(Integer, nullable=False)
    text: Mapped[Optional[str]] = mapped_column(String)
    tag: Mapped[Optional[str]] = mapped_column(String)

    mails: Mapped[list["Mail"]] = relationship(back_populates="mailing")

    def to_dict(self):
        return {field.name: getattr(self, field.name) for field in self.__table__.c}


class Mail(Base):
    __tablename__ = "mails"

    id: Mapped[int] = mapped_column(Integer, index=True, primary_key=True, autoincrement=True)
    send_datetime: Mapped[int] = mapped_column(Integer, nullable=False, default=func.now())
    status: Mapped[Optional[int]] = mapped_column(Integer, nullable=False, default=0)
    id_client: Mapped[int] = mapped_column(ForeignKey("clients.id"))
    id_mailing: Mapped[int] = mapped_column(ForeignKey("mailings.id"))

    client: Mapped["Client"] = relationship(back_populates="mails")
    mailing: Mapped["Mailing"] = relationship(back_populates="mails")

    def to_dict(self):
        return {field.name: getattr(self, field.name) for field in self.__table__.c}




