# NotificationService

## Иструкция к применению

1) Скачать проект
2) Установить python 3.9+, создать виртуальное окружение (venv) и  установить зависимости слудующей командой:
``` pip install requrements.txt```
3) Добавить токен доступа в файл config.py. А также конфигурацию smtp сервера (+ email).
4) Запустить проект комндой ```python main.py```


## Описание API

Описание в формате OpenAPI доступно по ссылке (при запущенном проекте) - localhost:8008/docs/ 



## Выполненные пукнкты доп. заданий:

5, 8, 9, 12


## Getting started


```
cd existing_repo
git remote add origin https://gitlab.com/VLco/notificationservice.git
git branch -M main
git push -uf origin main
```
