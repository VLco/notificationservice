import smtplib
import time
import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

import requests
from sqlalchemy.orm import sessionmaker

import config
from crud import *
from utils import engine, logger


def task_sending():
    while True:
        # init session db
        db = sessionmaker(autocommit=False, autoflush=False, bind=engine)()
        # now timestamp
        now_datetime = datetime.datetime.now().timestamp()

        # get all mailing by end_datetime
        mailings = get_mailings_by_filter(
            db,
            end_datetime=['>', int(now_datetime)],
            start_datetime=['<', int(now_datetime)]
        )

        if not mailings:
            return None
        for mailing in mailings:
            clients = get_client_by_filter(db, tag=['ilike', '%' + mailing.tag + '%'])
            for client in clients:
                mails = get_mails_by_filter(db, id_client=['==', client.id], id_mailing=['==', mailing.id])

                if not mails:
                    mail = schemas.MailCreate(id_client=client.id, id_mailing=mailing.id, status=0,
                                              send_datetime=int(datetime.datetime.now().timestamp()))
                    mail = create_mail(db, mail)

                elif mails[0].status == 0:
                    mail = mails[0]

                else:
                    continue

                if client.timezone:
                    zone = datetime.datetime.strptime(client.timezone, '%z').tzinfo
                    dt = datetime.datetime.now(tz=zone)
                    if dt.timestamp() > now_datetime:
                        continue

                res = send_message(mail.id, mailing.text, client.phone)

                if res:
                    mail.status = 1
                    m = mail.to_dict()
                    tmp = update_mail(db, schemas.Mail(**m))
                    logger.info('Send mail to client (client_id= {}, client_id = {}) '.format(mailing.id, client.id))
                else:
                    logger.warning(
                        'The message has not been sent (client_id= {}, client_id = {}) '.format(mailing.id, client.id)
                    )

        time.sleep(10)


def send_message(id, txt, phone):

    dict_msg = {
        'id': id,
        "phone": int(phone),
        "text": txt,
        'token': config.token
    }

    try:
        r = requests.post(f'https://probe.fbrq.cloud/v1/send/{id}', json=dict_msg, timeout=10)
        if r.ok and r.json()['code'] == 0:
            return True
        else:
            return False
    except Exception as err:
        logger.info(
            'An unknown error occurred when sending mail (mail_id = {}): {}'
            .format(id, err)
        )
        return False


def email_reports():
    # init session db
    db = sessionmaker(autocommit=False, autoflush=False, bind=engine)()

    all_count = len(get_mails_by_filter(db))
    success_count = len(get_mails_by_filter(db, status=['==', 1]))

    now_datetime = datetime.datetime.now().timestamp()

    title = 'Report for the day.'
    text = f'{title}\n\nAll mails - {all_count}\nAll success - {success_count}\n'
    for i in get_mailings_by_filter(db, start_datetime=['<', int(now_datetime)]):
        suc = len(get_mails_by_filter(db, id_mailing=['==', i.id], status=['==', 1]))
        unsuc = len(get_mails_by_filter(db, id_mailing=['==', i.id], status=['==', 1]))
        tmp = f'Mailing id = {i.id} | Success - {suc} | Unsuccessful - {unsuc}\n'

        text += tmp

    send_messages_email(title, text)


def send_messages_email(subject, body):
    smtp_conf = config.conf_smtp

    try:
        msg = MIMEMultipart()
        msg['From'] = smtp_conf.get('smtp_user')
        msg['To'] = config.email
        msg['Subject'] = '{}'.format(subject)
        msg['Date'] = formatdate(localtime=True)
        msg.attach(MIMEText(body, 'plain', _charset='utf-8'))
        content = msg.as_string()
        session = smtplib.SMTP(smtp_conf.get('smtp_host'), smtp_conf.get('smtp_port'), 60)

        if smtp_conf.get('encryption') == 'tls':
            session.starttls()
        elif smtp_conf.get('encryption') == 'ssl':
            pass  # TODO: ssl encryption

        session.login(smtp_conf.get('smtp_user'), smtp_conf.get('smtp_password'))
        session.sendmail(smtp_conf.get('smtp_user'), config.email, content)
        session.close()
        logger.info('An report has been sent')
    except Exception as e:
        logger.error('An error occurred sending mail: {}'.format(e))
