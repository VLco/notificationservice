import os
import logging

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists

from models import Base
from config import db_name

log_place = 'logs.log'
logging.basicConfig(
    filename=log_place,
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%d/%b/%Y:%H:%M:%S %z'
)

logger = logging.getLogger(__name__)

file_path = os.path.abspath(os.getcwd()) + f"/{db_name}"

SQLALCHEMY_DATABASE_URL = "sqlite:///" + file_path

# создание движка
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

if not database_exists(engine.url):
    Base.metadata.create_all(engine)


def get_db():
    db = sessionmaker(autocommit=False, autoflush=False, bind=engine)()
    try:
        yield db
    finally:
        db.close_all()


delete400 = {
    'description': 'Error',
    "content": {
        "application/json": {
            "example": {'status': 'error', 'detail': "Error deleting. See logs."}
        }
    }
}
delete200 = {
    'description': 'Success',
    "content": {
        "application/json": {
            "example": {'status': 'success'}
        }
    }
}
responses_for_delete = {200: delete200, 400: delete400}

stats400 = {
    'description': 'Error',
    "content": {
        "application/json": {
            "example": {'status': 'error', 'detail': "Error getting stats. See logs."}
        }
    }
}
stats200 = {
    'description': 'Success',
    "content": {
        "application/json": {
            "example": {
                'status': 'success',
                'stats': {
                    'All message': 0,
                    'All success messages': 0,
                    'Statistic': {}
                }
            }
        }
    }
}
responses_for_stats = {200: stats200, 400: stats400}




