from fastapi import APIRouter
from multiprocessing import Process
from apscheduler.schedulers.background import BackgroundScheduler


from tasks import task_sending, email_reports
from utils import logger

router = APIRouter()

proc = Process(target=task_sending)
scheduler = BackgroundScheduler()


@router.on_event("startup")
async def on_start_app():
    logger.info('App startup')

    # start cron
    scheduler.add_job(email_reports, 'cron', day=1)
    scheduler.start()

    # start task
    proc.start()


@router.on_event("shutdown")
async def on_end_app():

    # Shutdown
    proc.close()
    scheduler.shutdown()

    logger.info('App shutdown')



