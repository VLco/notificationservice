from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

import crud
from schemas import Mailing, MailingCreate
from utils import get_db, responses_for_delete


router = APIRouter()


@router.get("/mailing/{mailing_id}", tags=["mailings"], response_model=Mailing)
async def get_mailing(mailing_id: int, db: Session = Depends(get_db)):
    mailing = crud.get_mailing(db, mailing_id=mailing_id)
    return mailing


@router.post("/mailing/", tags=["mailings"], response_model=Mailing)
async def create_mailing(mailing: MailingCreate, db: Session = Depends(get_db)):
    c_mailing = crud.create_mailing(db=db, mailing=mailing)
    return c_mailing


@router.put("/mailing/", tags=["mailings"], response_model=Mailing)
async def update_mailing(mailing: Mailing, db: Session = Depends(get_db)):
    u_mailing = crud.update_mailing(db=db, update_data=mailing)
    return u_mailing


@router.delete("/mailing/{mailing_id}", tags=["mailings"], responses=responses_for_delete)
async def delete_mailing(mailing_id: int, db: Session = Depends(get_db)):
    d_mailing = crud.delete_mailing(db=db, mailing_id=mailing_id)
    if d_mailing:
        return JSONResponse(status_code=200, content={'status': 'success'})
    else:
        return JSONResponse(status_code=400, content={'status': 'error', 'detail': "Error deleting. See logs."})

