from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

import crud
from schemas import Client, ClientCreate
from utils import get_db, responses_for_delete


router = APIRouter()


@router.get("/client/{client_id}", tags=["clients"], response_model=Client)
async def get_client(client_id: int, db: Session = Depends(get_db)):
    client = crud.get_client(db, client_id=client_id)
    return client


@router.post("/client/", tags=["clients"], response_model=Client)
async def create_client(client: ClientCreate, db: Session = Depends(get_db)):
    db_client = crud.get_client_by_filter(db, phone=['==', client.phone])
    if db_client:
        return JSONResponse(status_code=400, content={'status': 'error', 'detail': "Phone already registered"})

    c_client = crud.create_client(db=db, client=client)
    return c_client


@router.put("/client/", tags=["clients"], response_model=Client)
async def update_client(client: Client, db: Session = Depends(get_db)):
    u_client = crud.update_client(db=db, update_data=client)
    return u_client


@router.delete("/client/{client_id}", tags=["clients"], responses=responses_for_delete)
async def delete_client(client_id: int, db: Session = Depends(get_db)):
    d_client = crud.delete_client(db=db, client_id=client_id)
    if d_client:
        return JSONResponse(status_code=200, content={'status': 'success'})
    else:
        return JSONResponse(status_code=400, content={'status': 'error', 'detail': "Error deleting. See logs."})
