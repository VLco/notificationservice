import datetime
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

import crud
from utils import get_db, responses_for_stats

router = APIRouter()


@router.get("/stats/", tags=["stats"], responses=responses_for_stats)
async def get_stats(db: Session = Depends(get_db)):
    all_count = len(crud.get_mails_by_filter(db))
    success_count = len(crud.get_mails_by_filter(db, status=['==', 1]))
    dict_stats = dict()
    now_datetime = datetime.datetime.now().timestamp()

    for i in crud.get_mailings_by_filter(db, start_datetime=['<', int(now_datetime)]):
        dict_stats[f'mailing id = {i.id}'] = {
            'success': len(crud.get_mails_by_filter(db, id_mailing=['==', i.id], status=['==', 1])),
            'unsuccess': len(crud.get_mails_by_filter(db, id_mailing=['==', i.id], status=['==', 0])),
        }

    response = {
        'status': 'success',
        'stats': {
            'All message': all_count,
            'Count success messages': success_count,
            'Statistic': dict_stats
        }
    }
    return JSONResponse(status_code=200, content=response)


@router.get("/stats/{mailing_id}", tags=["stats"], responses=responses_for_stats)
async def get_stats_by_mailing(mailing_id: int, db: Session = Depends(get_db)):
    all_mails = crud.get_mails_by_filter(db, id_mailing=['==', mailing_id])

    all_count = len(all_mails)
    success_count = len(crud.get_mails_by_filter(db, id_mailing=['==', mailing_id], status=['==', 1]))

    dict_stats = dict()

    for i in all_mails:
        dict_stats[f'client id = {i.id}'] = 'success' if i.status != 0 else 'unsuccess'

    response = {
        'status': 'success',
        'stats': {
            'All message': all_count,
            'Count success messages': success_count,
            'Statistic': dict_stats
        }
    }
    return JSONResponse(status_code=200, content=response)
