import datetime
from typing import Union
from pydantic import BaseModel


class ClientBase(BaseModel):
    phone: str
    phone_code: str
    tag: str
    timezone: Union[str, None] = None


class ClientCreate(ClientBase):
    pass


class Client(ClientBase):
    id: int

    class Config:
        from_attributes = True


class MailingBase(BaseModel):
    start_datetime: int
    end_datetime: int
    text: Union[str, None] = None
    tag: Union[str, None] = None


class MailingCreate(MailingBase):
    pass


class Mailing(MailingBase):
    id: int

    class Config:
        from_attributes = True


class MailBase(BaseModel):
    send_datetime: int
    status: Union[int, None] = None
    id_client: int
    id_mailing: int


class MailCreate(MailBase):
    pass


class Mail(MailBase):
    id: int

    class Config:
        from_attributes = True

